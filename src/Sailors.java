import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

class Sailors {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        HashMap<String, Integer> sailors = new HashMap<>();
        HashMap<Integer, List<Fruits>> fruits = new HashMap<>();
        HashMap<Integer, Fishes> fishes = new HashMap<>();

        List<Fruits> farr = new ArrayList<>();

        int x = sc.nextInt();

        int mangoes = 0, apples = 0, bananas = 0;
        int smallFish = 0, bigFish = 0;

            for (int i = 0; i < x; i++) {
                String ch = sc.next();

                if (ch.equals("A")) {
                    sailors.put("A", i);
                    System.out.println("Enter the number of mangoes : ");
                    mangoes = sc.nextInt();
                    System.out.println("Enter the number of apples : ");
                    apples = sc.nextInt();
                    System.out.println("Enter the number of bananas : ");
                    bananas = sc.nextInt();
                    farr.add(new Fruits(mangoes, apples, bananas));
                    fruits.put(1, farr);
                    continue;
                }

                if (ch.equals("B")) {
                    sailors.put("B", i);
                    System.out.println("Enter the number of small fishes : ");
                    smallFish = sc.nextInt();
                    System.out.println("Enter the number if big fishes : ");
                    bigFish = sc.nextInt();
                    fishes.put(i, new Fishes(smallFish, bigFish));
                }
            }

            System.out.println("Enter the group to be displayed : ");
            String ch = "";
        int id = 0;
        for (int i = 0; i < x; i++) {
            ch = sc.next();
            if (ch.equals("A")) {
                id = sailors.get("A");
                System.out.print(i + " " + fruits.get(id).get(i).getMangoes() + " ");
            }
            if (ch.equals("B")) {
                id = sailors.get("B");
                System.out.print(i + " " + fishes.get(id).getBigFish() + " ");
                System.out.println();
            }
        }
    }
}