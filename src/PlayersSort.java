import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class PlayersSort {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int n = 0;
        boolean typeError = true;

        while (typeError) {
            System.out.println("Enter the number of players : ");
            if (sc.hasNextInt()) {
                n = sc.nextInt();
            }else {
                System.out.println("Input is not a valid number :(");
                sc.next();
            }
            typeError = false;
        }

        ArrayList<Player> players = new ArrayList<>();

        String name;
        int score;
        typeError = true;

        for (int i = 0; i < n; i++) {
            System.out.println("Enter the nam of the player : ");
            name = sc.next();
            score = 0;
            typeError = true;
            while(typeError) {
                System.out.println("Enter a Score : ");
                if (sc.hasNextInt()) {
                    score = sc.nextInt();
                } else {
                    System.out.println("Input score is not a valid number :(");
                    sc.next();
                    continue;
                }
                typeError = false;
            }
            players.add(new Player(name, score));
        }

        System.out.println("Enter a sorting param : ");
        System.out.println("1. Score 2. Name");


        int loop = 1;

        if (loop == 1) {
            int option = sc.nextInt();
            switch (option) {
                case 1:
                    Collections.sort(players);
                    break;
                case 2:
                    Collections.sort(players, Comparator.comparing(Player::getName));
            }
            System.out.println("Enter 1 to continue : ");
            loop = sc.nextInt();
        }



        for (Player player : players) {
            System.out.println("Player Name : " + player.getName());
            System.out.println("Player Score : " + player.getScore());
        }

    }

}
