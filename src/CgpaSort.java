import java.util.*;

public class CgpaSort {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int n = 0;
        boolean typeError = true;

        while (typeError) {
            System.out.println("Enter the number of records : ");
            if (sc.hasNextInt()) {
                n = sc.nextInt();
            }else {
                System.out.println("Input is not a valid number :(");
                sc.next();
            }
            typeError = false;
        }

        ArrayList<Student> students = new ArrayList<>();

        String name;
        float cgpa;
        typeError = true;

        for (int i = 0; i < n; i++) {
            System.out.println("Enter the name for record " + i);
            name = sc.next();
            cgpa = 0;
            typeError = true;
            while (typeError) {
                System.out.println("Enter the CGPA for record " + i);
                if (sc.hasNextFloat()) {
                    cgpa = sc.nextFloat();
                }else {
                    System.out.println("Input is not a valid decimal :(");
                    sc.next();
                    continue;
                }
                typeError = false;
            }
            students.add(new Student(i, name, cgpa));
        }

        Collections.sort(students, cgpaComparator);

        for (int i = 0; i < n; i++) {
            System.out.print(
                    "Student ID " + students.get(i).getId() +
                            " Student First Name " + students.get(i).getFirstName() +
                            " CGPA " + students.get(i).getCgpa()
            );
            System.out.println();
        }

    }

    public static Comparator<Student> cgpaComparator = new Comparator<Student>() {
        @Override
        public int compare(Student o1, Student o2) {
            if (o1.getCgpa() > o2.getCgpa()) {
                return -1;
            }else if (o1.getCgpa() < o2.getCgpa()) {
                return 1;
            }else if (o1.getFirstName().equals(o2.getFirstName())) {
                return o1.getId() - o2.getId();
            }
            return o1.getFirstName().compareTo(o2.getFirstName());
        }
    };

}

