import java.util.HashMap;
import java.util.Scanner;

public class PhoneBook {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        HashMap<String, Long> book = new HashMap<>();
        String name;
        long number;
        boolean typeError = true;

        int loop = 1;
        while (loop == 1) {

            int n = 0;
            typeError = true;

            while (typeError) {
                System.out.println("Enter the number of records : ");
                if (sc.hasNextInt()) {
                    n = sc.nextInt();
                }else {
                    System.out.println("Input is not a valid number :(");
                    sc.next();
                }
                typeError = false;
            }



            for (int i = 0; i < n; i++) {
                System.out.println("Enter a name for the record " + i);
                name = sc.next();
                number = 0;
                typeError = true;
                while (typeError) {
                    System.out.println("Enter a number for the record : " + i);
                    if (sc.hasNextLong()) {
                        number = sc.nextLong();
                    }else {
                        System.out.println("Input is not a valid number :(");
                        sc.next();
                        continue;
                    }
                    typeError = false;
                }
                book.put(name, number);
            }

            System.out.println("Enter 1 to continue : ");
            loop = sc.nextInt();
        }

        loop = 1;

        while (loop == 1) {
            System.out.println("Enter the name of the record to display the phone number : ");

            name = sc.next();

            if (book.get(name) == null) {
                System.out.println("Record not found :(");
            }else {
                System.out.println(book.get(name));
            }

            System.out.println("Enter 1 to continue : ");
            loop = sc.nextInt();

        }
    }

}
