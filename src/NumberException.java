public class NumberException extends Exception {

    private int number;

    NumberException(int number) {
        this.number = number;
    }
}
