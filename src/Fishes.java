public class Fishes {

    int smallFish;
    int bigFish;

    public int getSmallFish() {
        return smallFish;
    }

    public void setSmallFish(int smallFish) {
        this.smallFish = smallFish;
    }

    public int getBigFish() {
        return bigFish;
    }

    public void setBigFish(int bigFish) {
        this.bigFish = bigFish;
    }

    public Fishes(int smallFish, int bigFish) {
        this.smallFish = smallFish;
        this.bigFish = bigFish;
    }
}
