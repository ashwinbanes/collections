import java.util.*;

public class Sets {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        Set<String> sets = new HashSet<>();
        List<Integer> list = new ArrayList<>();

        int n = 0;
        boolean typeError = true;

        while (typeError) {
            System.out.println("Enter the number of records : ");
            if (sc.hasNextInt()) {
                n = sc.nextInt();
            }else {
                System.out.println("Input is not a valid number :(");
                sc.next();
            }
            typeError = false;
        }

        String value;

        sc.nextLine();
        for (int i = 0; i < n; i++) {
            value = sc.nextLine();
            sets.add(value);
            list.add(sets.size());
        }

        for (int size : list) {
            System.out.println(size);
        }

    }

}
