public class Calculator {

    public static long pow(int n, int p) throws NumberException {

        if (n < 0 || n == 0) {
            throw new NumberException(n);
        }else if (p < 0 || p == 0){
            throw new NumberException(p);
        }else {
            return (long)Math.pow(n, p);
        }
    }

}
