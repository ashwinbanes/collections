import java.util.Scanner;

public class PowNumbers {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n = 0, p = 0;
        int loop = 1;
        boolean typeError = true;

            while (typeError) {
                System.out.println("Enter the base number : ");
                if (sc.hasNextInt()) {
                    n = sc.nextInt();
                }else {
                    sc.next();
                    System.out.println("Base number is not a valid number :(");
                }
                typeError = false;
            }

            typeError = true;

            while (typeError) {
                System.out.println("Enter the pow number : ");
                if (sc.hasNextInt()) {
                    p = sc.nextInt();
                }else {
                    sc.next();
                    System.out.println("Power number is not a valid number :(");
                }
                typeError = false;
            }

            try {
                System.out.println(Calculator.pow(n , p));
            } catch (NumberException e) {
                e.printStackTrace();
            }



    }

}
