import java.util.HashMap;
import java.util.Scanner;

public class UniqueIntSub {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int n = 0, m = 0, count = 0;
        HashMap<Integer, Integer> map = new HashMap<>();
        boolean typeError = true;

        while (typeError) {
            System.out.println("Enter the number of elements : ");
            if (sc.hasNextInt()) {
                n = sc.nextInt();
            }else {
                sc.next();
                System.out.println("Number is not a valid type :(");
            }
            typeError = false;
        }

        int[] arr = new int[n];

        typeError = true;

        while (typeError) {
            System.out.println("Enter the number of sub array elements : ");
            if (sc.hasNextInt()) {
                m = sc.nextInt();
            }else {
                sc.next();
                System.out.println("Number is not a valid type :(");
            }
            typeError = false;
        }

        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }

        for (int i = 0; i < n - m; i++) {
            int curr = 0;
            for (int k = i; k < i + m; k++) {
                if (!map.containsKey(arr[k])) {
                    map.put(arr[i] , i);
                    curr++;
                }
            }
            if (curr > count) {
                count = curr;
            }
        }

        System.out.println(count);
        System.out.println(map);

    }

}
