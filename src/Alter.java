import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Alter {

    static HashMap<String, Fruits> fruits = new HashMap<>();
    static HashMap<String, Fishes> fishes = new HashMap<>();
    static List<HashMap<String, Fruits>> sailorsA = new ArrayList<>();
    static List<HashMap<String, Fishes>> sailorsB = new ArrayList<>();

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int x = sc.nextInt();
        String option = "";
        int mangoes, apples, bananas;
        int smallFish, bigFish;

        for (int i = 0; i < x; i++) {
            System.out.println("Enter an option for Sailor " + i);
            option = sc.next();
            if (option.equals("A")) {
                fruits = new HashMap<>();
                System.out.println("Enter number of Mangoes : ");
                mangoes = sc.nextInt();
                System.out.println("Enter number of apples : ");
                apples = sc.nextInt();
                System.out.println("Enter number of bananas : ");
                bananas = sc.nextInt();
                fruits.put("A", new Fruits(mangoes, apples, bananas));
                sailorsA.add(fruits);
                continue;
            }
            if (option.equals("B")) {
                fishes = new HashMap<>();
                System.out.println("Enter the number of small fishes : ");
                smallFish = sc.nextInt();
                System.out.println("Enter the number of big fishes : ");
                bigFish = sc.nextInt();
                fishes.put("B", new Fishes(smallFish, bigFish));
                sailorsB.add(fishes);
            }
        }

        System.out.println("Enter the option to display : ");

        int loop = 1;

        while (loop == 1) {
            System.out.println("1. Display A, 2. Display B, 3. Display Both, 4. Total Fruits per sailor," +
                    "5. Total fishes per sailor, 6. Total fruits by group A, " +
                    "7. Total Fishes by group B");
            option = sc.next();
            switch (option) {
                case "1":
                    dispA();
                    break;
                case "2":
                    dispB();
                    break;
                case "3":
                    dispBoth();
                    break;
                case "4":
                    eachA();
                    break;
                case "5":
                    eachB();
                    break;
                case "6":
                    totalA();
                    break;
                case "7":
                    totalB();
                    break;
            }
            System.out.println("Enter 1 to continue : ");
            loop = sc.nextInt();
        }

    }

    private static void totalB() {
        int total = 0;
        for (int i = 0; i < sailorsB.size(); i++) {
            total = total + sailorsB.get(i).get("B").getBigFish() + sailorsB.get(i).get("B").getSmallFish();
        }

        System.out.println("Total fishes collected by sailor is" +
                total);

    }

    private static void totalA() {
        int total = 0;
        for (int i = 0; i < sailorsA.size(); i++) {
            total = total + sailorsA.get(i).get("A").getMangoes() + sailorsA.get(i).get("A").getApples() +
                    sailorsA.get(i).get("A").getBananas();
        }
        System.out.println("Total fruits collected by sailors is" +
                total);
    }

    private static void eachB() {
        int total = 0;
        for (int i = 0; i < sailorsB.size(); i++) {
            total = sailorsB.get(i).get("B").getBigFish() + sailorsB.get(i).get("B").getSmallFish();
            System.out.println("Total fishes collected by sailor " + i + " is" +
                    total);

        }
    }

    private static void eachA() {
        int total = 0;
        for (int i = 0; i < sailorsA.size(); i++) {
            total = sailorsA.get(i).get("A").getMangoes() + sailorsA.get(i).get("A").getApples() +
                    sailorsA.get(i).get("A").getBananas();
            System.out.println("Total fruits collected by sailor " + i + " is" +
                    total);

        }
    }

    private static void dispBoth() {
        System.out.println("Group A Sailors");
        for (int i = 0; i < sailorsA.size(); i++) {
            System.out.println(sailorsA.get(i).get("A").getMangoes());
        }
        System.out.println("Group B Sailors");
        for (int i = 0; i < sailorsB.size(); i++) {
            System.out.println(sailorsB.get(i).get("B").getBigFish());
        }
    }

    private static void dispB() {
        for (int i = 0; i < sailorsB.size(); i++) {
            System.out.println(sailorsB.get(i).get("B").getBigFish());
        }
    }

    private static void dispA() {
        for (int i = 0; i < sailorsA.size(); i++) {
            System.out.println(sailorsA.get(i).get("A").getMangoes());
        }
    }

}
