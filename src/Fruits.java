class Fruits {
    int mangoes;
    int apples;
    int bananas;

    public int getMangoes() {
        return mangoes;
    }

    public void setMangoes(int mangoes) {
        this.mangoes = mangoes;
    }

    public int getApples() {
        return apples;
    }

    public void setApples(int apples) {
        this.apples = apples;
    }

    public int getBananas() {
        return bananas;
    }

    public void setBananas(int bananas) {
        this.bananas = bananas;
    }

    public Fruits(int mangoes, int apples, int bananas) {
        this.mangoes = mangoes;
        this.apples = apples;
        this.bananas = bananas;
    }
}